package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
	String serverName;
	String port;
	String dbName;
	String dbUser;
	String dbPassword;

	public MyConnection() {
		this.serverName = "localhost";
		this.port = "3306";
		this.dbName = "shopping";
		this.dbUser = "root";
		this.dbPassword = "";
	}

	public MyConnection(String serverName, String port, String dbName, String dbUser, String dbPassword) {
		super();
		this.serverName = serverName;
		this.port = port;
		this.dbName = dbName;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
	}

	public Connection getConnection() {
		Connection connection = null;

		String dbUrl = "jdbc:mysql://" + this.serverName + ":" + this.port + "/" + this.dbName;
		System.out.println(dbUrl);

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		System.out.println(connection);
		return connection;
	}
}
