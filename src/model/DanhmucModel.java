package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.MyConnection;
import entities.Danhmuc;

public class DanhmucModel {
	Danhmuc dm;
	public DanhmucModel(){
		
	}
	public DanhmucModel(Danhmuc dm){
		this.dm=dm;
		
	}
	public ArrayList<Danhmuc> getList(){
		ArrayList<Danhmuc> list =new ArrayList<>();
		Connection connection= new MyConnection().getConnection();
		if(connection==null)
			return null;
		try{
			String sql = "select * from danhmuc";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Danhmuc temp= new Danhmuc(rs.getInt(1),rs.getString(2));
				list.add(temp);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return list;
	}
}
