package model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Part;

import controller.MyConnection;
import entities.Product;

public class ProductModel {
	Product sanpham;
	Part file;

	public ProductModel(Part file) {
		this.file = file;
	}

	public ProductModel() {

	}

	public ProductModel(Product sanpham) {
		this.sanpham = sanpham;
	}

	public ArrayList<Product> getList() {
		ArrayList<Product> list = new ArrayList<>();
		Connection connection = new MyConnection().getConnection();
		if (connection == null) {
			return null;
		}
		try {
			String sql = "select * from sanpham";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Product temp = new Product(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4),rs.getInt(5));
				list.add(temp);
			}
			ps.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Product getByProductByMasp(String masp) {
		Connection connection = new MyConnection().getConnection();
		Product pro = null;
		if (connection == null) {
			return null;
		}
		try {
			String sql = "select * from sanpham where masp=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, masp);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				pro = new Product(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4));
			}
			ps.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return pro;
	}

	public String getFileName(Part filepart) {
		String filename = "";
		String header = filepart.getHeader("Content-Disposition");
		// System.out.println("header:"+header);
		int beginIndex = header.lastIndexOf("=");
		filename = header.substring(beginIndex);
		Pattern p = Pattern.compile("\"([^\"]*)\"");
		Matcher m = p.matcher(filename);
		while (m.find()) {
			filename = m.group(1);
			beginIndex = filename.lastIndexOf("\\");
			filename = filename.substring(beginIndex + 1);

		}
		return filename;

	}

	public void uploadFile(String uploadRootPath) throws IOException {
		try {
			InputStream fis = file.getInputStream();
			byte[] data = new byte[fis.available()];
			fis.read();
			FileOutputStream out = new FileOutputStream(new File(uploadRootPath + "\\" + getFileName(file)));
			out.write(data);
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("That bai");
		}
		System.out.println("Thanh cong");
	}
}
