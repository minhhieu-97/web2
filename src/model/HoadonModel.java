package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import controller.MyConnection;
import entities.Hoadon;

public class HoadonModel {
	Hoadon hoadon;

	public HoadonModel(Hoadon hoadon) {
		this.hoadon = hoadon;
	}
	public int insertHoadon(){
		int kq=0;
		Connection connection = new MyConnection().getConnection();
		if(connection==null)
			return 0;
		try{
			String sql="insert into hoadon(ngaydh) values(?)";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, hoadon.getNgaydh());
			kq= ps.executeUpdate();
		}catch(SQLException e ){
			e.printStackTrace();
		}
		return kq;
	}
	public int getNewestIdHoadon(){
		Connection connection =new MyConnection().getConnection();
		int MAXID=0;
		if(connection==null)
			return 0;
		try{
			String sql ="select MAX(MAHD) from HOADON";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				MAXID=rs.getInt(1);
			}
			ps.close();
			connection.close();
			
		}catch(SQLException e ){
			e.printStackTrace();
		}
		return MAXID;
	}
	
	
}
