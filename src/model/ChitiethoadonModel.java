package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import controller.MyConnection;
import entities.Chitiethoadon;

public class ChitiethoadonModel {
Chitiethoadon cthd;

public ChitiethoadonModel(Chitiethoadon cthd) {
	this.cthd = cthd;
}

public int insertChitietHoaDon(){
	int kq=0;
	Connection connection =new MyConnection().getConnection();
	if(connection==null)
		return 0;
	try{
		String sql="insert into CHITIETHOADON(mahd, masp, soluong) values(?,?,?)";
		PreparedStatement ps =connection.prepareStatement(sql);
		ps.setInt(1, cthd.getMahd());
		ps.setString(2, cthd.getMasp());
		ps.setInt(3, cthd.getSoluong());
		kq=ps.executeUpdate();
	}catch(SQLException e ){
		e.printStackTrace();
	}
	
	return kq;
}

}
